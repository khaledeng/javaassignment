Task

To develop an API which is able to do the below activities.

 1. Create a new cart for a Registered user/customer
 2. Create a cart entry for the cart of the user/customer
 3. View cart entry of the cart of the user/customer
 
Questions you can ask yourself

1.  What is Ecommerce/Available Store?
    Store is the online representation of the available stores attached to a company or country. This about how you can comibine this with your APIs

2.  What is Cart ?
    Cart is representation of the items a customer selected and would like to purchase.
    
3.  What is Cart Entry ?
    Cart Entry is representation of a single item a customer  would like to purchase which is present in his Cart.


You are free to add any attributes to the entities like Cart,Store,Cart Entry,Address etc that fits the application and is relevant to the task.

You can use the boiler plate code, to start the application follow the below steps or improve the boiler plate with code of your own. 

***Spring boot task based on Hibernate with Maven**

1. Navigate to the project folder and perform the below actions

Pre requsite is to install maven

 ```
    brew install maven
 ```
Run the command to build the application
```
    mvn install
```
Start the server
```
   mvn spring-boot:run
 ```

2. Access http://localhost:8080/api/carts in the browser of choice


If you want to try out the application with docker you can use the below instructions to do so.(nice to have)

**Spring with Docker**

1.  Deploy the application as a docker container using HELM or docker-compose
2.  Prepare a deployment for minikube(to simulate kubernates)